/*This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

sserv version 1.0 Desarrollado por Alexander Lopez P. Copyright 2012. alexanderlopezp@gmail.com*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <resolv.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

/* la tasa de baudios esta definida en <asm/termbits.h>, que estaincluida <termios.h> */
#define BAUDRATE B9600
/* cambie esta definicion por el puerto correcto */
#define MODEMDEVICE "/dev/balanza"
#define _POSIX_SOURCE 1 /* fuentes cumple POSIX */
#define FALSE 0
#define TRUE 1

/** Puerto  */
#define PORT       2029

/** Número máximo de hijos */
#define MAX_CHILDS 100

/** Longitud del buffer  */
#define BUFFERSIZE 255

void getPeso(char *peso);
void conectar(void);
int AtiendeCliente(int socket, struct sockaddr_in addr);
void error(int code, char *err);
void reloj(int loop);
int DemasiadosClientes(int socket, struct sockaddr_in addr);

int main(){
  printf("sserv version 1.0 Desarrollado por Alexander Lopez P. Copyright 2012. alexanderlopezp@gmail.com\n\n");
  conectar();
  return 0;
}

void getPeso(char *peso){
  int fd,c, res;
  struct termios oldtio,newtio;
  char buf[255];
  int c_b;
  int d;

  /*Abre el dispositivo modem para lectura y escritura y no como controladortty porque no queremos que nos mate si el ruido de la linea enviaun CTRL-C.*/
  fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY );
  if (fd <0) {
	perror(MODEMDEVICE);
	exit(-1);
  }	

  tcgetattr(fd,&oldtio);

  /* almacenamos la configuracion actual del puerto */
  bzero(&newtio, sizeof(newtio));

  /* limpiamos struct para recibir losnuevos parametros del puerto */

  /*
  BAUDRATE: Fija la tasa bps. Podria tambien usar cfsetispeed y cfsetospeed.
  CRTSCTS : control de flujo de salida por hardware (usado solo si el cabletiene todas las lineas necesarias Vea sect. 7 de Serial-HOWTO)
  CS8: 8n1 (8bit,no paridad,1 bit de parada)
  CLOCAL : conexion local, sin control de modem
  CREAD: activa recepcion de caracteres
  */


  newtio.c_cflag = BAUDRATE | CS8 | CREAD;

  /*
  IGNPAR : ignora los bytes con error de paridad
  ICRNL: mapea CR a NL (en otro caso una entrada CR del otro ordenadorno terminaria la entrada) en otro caso hace un dispositivo en bruto(sin otro proceso de entrada)
  */

  newtio.c_iflag = IGNPAR | ICRNL;

  /*Salida en bruto.*/
  newtio.c_oflag = 0;

  /*
  ICANON : activa entrada canonica desactiva todas las funcionalidades del eco, y no envia segnales alprogramallamador
  */

  newtio.c_lflag = ICANON;

  /*inicializa todos los caracteres de controllos valores por defecto se pueden encontrar en /usr/include/termios.h,y vienen dadas en los comentarios, pero no los necesitamos aqui  */

  newtio.c_cc[VINTR]= 0;	/* Ctrl-c */
  newtio.c_cc[VQUIT]= 0;	/* Ctrl-\ */
  newtio.c_cc[VERASE]= 0;	/* del */
  newtio.c_cc[VKILL]= 0;	/* @ */
  newtio.c_cc[VEOF]= 4;	/* Ctrl-d */
  newtio.c_cc[VTIME]= 0;	/* temporizador entre caracter, no usado */
  newtio.c_cc[VMIN]= 1;	/* bloqu.lectura hasta llegada de caracter. 1 */
  newtio.c_cc[VSWTC]= 0;	/* ’\0’ */
  newtio.c_cc[VSTART]= 0;	/* Ctrl-q */
  newtio.c_cc[VSTOP]= 0;	/* Ctrl-s */
  newtio.c_cc[VSUSP]= 0;	/* Ctrl-z */
  newtio.c_cc[VEOL]= 0;	/* ’\0’ */
  newtio.c_cc[VREPRINT] = 0;	/* Ctrl-r */
  newtio.c_cc[VDISCARD] = 0;	/* Ctrl-u */
  newtio.c_cc[VWERASE] = 0;	/* Ctrl-w */
  newtio.c_cc[VLNEXT]= 0;	/* Ctrl-v */
  newtio.c_cc[VEOL2]= 0;	/* ’\0’ */

/*ahora limpiamos la linea del modem y activamos la configuracion delpuerto*/
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);

  /*configuracion del terminal realizada, ahora manejamos las entradas.En este ejemplo, al introducir una ’z’ al inicio de linea terminara elprograma.*/
  //while (STOP==FALSE) {	/* bucle hasta condicion de terminar */
	/*bloque de ejecucion de programa hasta que llega un caracter de fin delinea, incluso si llegan mas de 255 caracteres.
	Si el numero de caracteres leidos es menor que el numero de caracteresdisponibles, las siguientes lecturas devolveran los caracteres restantes.
	’res’ tomara el valor del numero actual de caracteres leidos.*/
	//fcntl(fd, F_SETFL, FNDELAY);
	c_b = write(fd,"P\r",1);
	if(c_b==-1){
		printf("Error al enviar!");
	}else{
		//printf("%d", c_b);
	}
	//while(res == -1){
		res = read(fd,buf,255);
		//sleep(1000);
		//d++;
		//if(d==5000){
			//break;
		//}
	//}
	buf[res]='\0';
	/* envio de fin de cadena, a fin de poder usar printf */
	//printf(":%s:%d\n", buf, res);
	//printf("%s\n",buf);
	//if (buf[0]=='z'){
	//	STOP=TRUE;
	//}
  //}
  /* restaura la anterior configuracion del puerto */
  tcsetattr(fd,TCSANOW,&oldtio);
  close(fd);
  strcpy(peso, buf);
}

void conectar(void){
  int socket_host;
    struct sockaddr_in client_addr;
    struct sockaddr_in my_addr;
    struct timeval tv;      /* Para el timeout del accept */
    socklen_t size_addr = 0;
    int socket_client;
    fd_set rfds;        /* Conjunto de descriptores a vigilar */
    int childcount=0;
    int exitcode;

    int childpid;
    int pidstatus;

    int activated=1;
    int loop=0;
    socket_host = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_host == -1)
      error(1, "No puedo inicializar el socket");
    
    my_addr.sin_family = AF_INET ;
    my_addr.sin_port = htons(PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY ;

    
    if( bind( socket_host, (struct sockaddr*)&my_addr, sizeof(my_addr)) == -1 )
      error(2, "El puerto está en uso"); /* Error al hacer el bind() */

    if(listen( socket_host, 10) == -1 )
      error(3, "No puedo escuchar en el puerto especificado");

    size_addr = sizeof(struct sockaddr_in);


    while(activated)
      {
    //reloj(loop);
    /* select() se carga el valor de rfds */
    FD_ZERO(&rfds);
    FD_SET(socket_host, &rfds);

    /* select() se carga el valor de tv */
    tv.tv_sec = 0;
    tv.tv_usec = 500000;    /* Tiempo de espera */
    
    if (select(socket_host+1, &rfds, NULL, NULL, &tv))
      {
        if((socket_client = accept( socket_host, (struct sockaddr*)&client_addr, &size_addr))!= -1)
          {
        loop=-1;        /* Para reiniciar el mensaje de Esperando conexión... */
        printf("\nSe ha conectado %s por su puerto %d\n", inet_ntoa(client_addr.sin_addr), client_addr.sin_port);
        switch ( childpid=fork() )
          {
          case -1:  /* Error */
            error(4, "No se puede crear el proceso hijo");
            break;
          case 0:   /* Somos proceso hijo */
            if (childcount<MAX_CHILDS)
              exitcode=AtiendeCliente(socket_client, client_addr);
            else
              exitcode=DemasiadosClientes(socket_client, client_addr);

            exit(exitcode); /* Código de salida */
          default:  /* Somos proceso padre */
            childcount++; /* Acabamos de tener un hijo */
            close(socket_client); /* Nuestro hijo se las apaña con el cliente que 
                         entró, para nosotros ya no existe. */
            break;
          }
          }
        else
          fprintf(stderr, "ERROR AL ACEPTAR LA CONEXIÓN\n");
      }

    /* Miramos si se ha cerrado algún hijo últimamente */
    childpid=waitpid(0, &pidstatus, WNOHANG);
    if (childpid>0)
      {
        childcount--;   /* Se acaba de morir un hijo */

        /* Muchas veces nos dará 0 si no se ha muerto ningún hijo, o -1 si no tenemos hijos
         con errno=10 (No child process). Así nos quitamos esos mensajes*/

        if (WIFEXITED(pidstatus))
          {

        /* Tal vez querremos mirar algo cuando se ha cerrado un hijo correctamente */
        if (WEXITSTATUS(pidstatus)==99)
          {
            printf("\nSe ha pedido el cierre del programa\n");
            activated=0;
          }
          }
      }
    loop++;
    }

    close(socket_host);
}

int AtiendeCliente(int socket, struct sockaddr_in addr)
{

    char buffer[BUFFERSIZE];
    char aux[BUFFERSIZE];
    char peso[255];
    int bytecount;
    int fin=0;
    int code=0;         /* Código de salida por defecto */
    time_t t;
    struct tm *tmp;

    while (!fin)
      {

    memset(buffer, 0, BUFFERSIZE);
    if((bytecount = recv(socket, buffer, BUFFERSIZE, 0))== -1)
      error(5, "No puedo recibir información");

    /* Evaluamos los comandos */
    /* El sistema de gestión de comandos es muy rudimentario, pero nos vale */
    /* Comando TIME - Da la hora */
    /*if (strncmp(buffer, "TIME", 4)==0)
      {
        memset(buffer, 0, BUFFERSIZE);

        t = time(NULL);
        tmp = localtime(&t);

        strftime(buffer, BUFFERSIZE, "Son las %H:%M:%S\n", tmp);
      }
    /* Comando DATE - Da la fecha */
    /*else if (strncmp(buffer, "DATE", 4)==0)
      {
        memset(buffer, 0, BUFFERSIZE);

        t = time(NULL);
        tmp = localtime(&t);

        strftime(buffer, BUFFERSIZE, "Hoy es %d/%m/%Y\n", tmp);
      }
    /* Comando HOLA - Saluda y dice la IP */
    /*else if (strncmp(buffer, "HOLA", 4)==0)
      {
        memset(buffer, 0, BUFFERSIZE);
        sprintf(buffer, "Hola %s, ¿cómo estás?\n", inet_ntoa(addr.sin_addr));
      }
    /* Comando EXIT - Cierra la conexión actual */
    /*else if (strncmp(buffer, "EXIT", 4)==0)
      {
        memset(buffer, 0, BUFFERSIZE);
        sprintf(buffer, "Hasta luego. Vuelve pronto %s\n", inet_ntoa(addr.sin_addr));
        fin=1;
      }
    /* Comando CERRAR - Cierra el servidor */
    /*else if (strncmp(buffer, "CERRAR", 6)==0)
      {
        memset(buffer, 0, BUFFERSIZE);
        sprintf(buffer, "Adiós. Cierro el servidor\n");
        fin=1;
        code=99;        /* Salir del programa */
      //}
    /*else
      {     
        sprintf(aux, "ECHO: %s\n", buffer);
        strcpy(buffer, aux);
      }

    if((bytecount = send(socket, buffer, strlen(buffer), 0))== -1)
      error(6, "No puedo enviar información");*/
    
	buffer[bytecount]='\0';
	//printf("%s\n", buffer1);
      
	if(buffer[0]=='P'){
	  getPeso(peso);
	  //formatearPeso(peso);
	  //printf("%d: %s\n", strlen(peso), peso);
	  write(socket, peso, strlen(peso));
	  //write(socket_host,"  2.850 kg ", strlen("  2.850 kg "));
	}
      
	if(strcmp(buffer,"salir")==1){
	  break;
	}
    
      }

    close(socket);
    return code;
}

int DemasiadosClientes(int socket, struct sockaddr_in addr)
{
    char buffer[BUFFERSIZE];
    int bytecount;

    memset(buffer, 0, BUFFERSIZE);
    
    sprintf(buffer, "Demasiados clientes conectados. Por favor, espere unos minutos\n");

    if((bytecount = send(socket, buffer, strlen(buffer), 0))== -1)
      error(6, "No puedo enviar información");
    
    close(socket);

    return 0;
}
